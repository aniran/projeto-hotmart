FROM cgswong/aws

WORKDIR /var/ambiente

RUN apk update && \
    apk upgrade && \
    apk add bash && \
    apk add --update ncurses-libs && \
    apk add --update dialog && \
    apk add openssl && \
    apk add terraform && \
    apk add python python-dev py-pip && \
    apk --update add gcc make libffi-dev musl-dev openssl-dev perl sshpass && \
    pip install ansible && \
    pip install --upgrade pip && \
    ansible-galaxy install geerlingguy.nodejs && \
    ansible-galaxy install geerlingguy.docker && \
    ansible-galaxy install geerlingguy.java 

RUN mkdir -p /root/.ssh /root/.aws  && \
    chmod go-rwx /root/.ssh /root/.aws

CMD ["/var/ambiente/run_menu.sh"]
