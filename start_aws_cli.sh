#!/bin/bash

git --version 
if [ $? -ne 0 ]
then
    echo "git nao encontrado."
    exit 1
fi

docker --version
if [ $? -ne 0 ]
then
    echo "Docker engine nao encontrado."
    exit 1
fi

git pull

docker build --rm -t aws-cli -f Dockerfile .

docker run -ti --rm --name aws-cli -v ${PWD}/dashboard:/var/ambiente aws-cli $1

#read -p "Pressione ENTER para sair"
