# Ambiente Cloud

O script pode ser executado em plataforma Windows/Linux.

# Dependências

- [Docker CE](https://store.docker.com/search?type=edition&offering=community) versão 17.0.0 (ou superior) - para executar o toolkit do provedor cloud.
- [Git](https://git-scm.com/downloads) - controle de versão.
- Conta da [Amazon AWS](https://aws.amazon.com).

## Configuração

- Crie um novo usuário no console da AWS.
![](./github_landing/novo_usuario.png)
- Certifique-se de marcar a opção **Programmatic Access**
![](./github_landing/programmatic_access.png)
- Inclua as permissões:
```
SystemAdministrator
NetworkAdministrator
PowerUserAccess
```
- Lembre-se de salvar o **Access key ID** e o **Secret Access key**.
![](./github_landing/salvar_access_secret_key.png)
- Execute o script **start_aws_cli.bat** ou **start_aws_cli.sh** para iniciar o *dashboard*.
- Serão solicitadas pelo `aws configure` as credenciais *AWS_ACCESS_KEY_ID* e *AWS_SECRET_ACCESS_KEY* para controlar os recursos.
- Será solicitado que o usuário crie uma senha de acesso ao *dashboard*. 
Esta senha não é armazenada em lugar algum. O script a utiliza para criptografar as credenciais AWS armazenadas fora da imagem Docker.
- O script criará um par de chaves RSA para acessar as instâncias EC2. 
Elas estarão disponíveis fora do conteiner na pasta **/ssh_keys** ao fim do `terraform apply`. 

### Problemas conhecidos

Se estiver usando Docker no Windows desabilite o autocrlf **antes** de realizar o clone :
```bash
git config --global core.autocrlf false
```
Isto corrige um [problema recorrente](https://github.com/docker/labs/issues/215) em containers rodando em Docker Windows usando scripts bash.
