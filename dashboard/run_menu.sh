#!/bin/sh

HOME_DASH=/var/ambiente
CMD_TF=$1

if [ -z "${CMD_TF}" ]
then
	CMD_TF=apply
fi

source set_envs.sh

terraform init
terraform ${CMD_TF}

if [ "${CMD_TF}" != "destroy" ]
then
	python build_tf_inventory.py

	if [ -f /root/.ssh/known_hosts ]
	then
		rm /root/.ssh/known_hosts
	fi

	for ll in $(grep 'INICIO_HOSTS_ANSIBLE' /etc/hosts -A999 | grep -v 'INICIO_HOSTS_ANSIBLE' | awk '{print $2}')
	do
        EXISTE_HOSTS="1"
	    ssh-keygen -q -F $ll 1>/dev/null
	    if [ "$?" -ne 0 ]
	    then 
	        ssh-keyscan -H $ll >> /root/.ssh/known_hosts
	    fi 
	done

    if [ ! -z "${EXISTE_HOSTS}" ]
    then
        ./provisioner.sh
        cp -f /root/.aws/* ${HOME_DASH}/aws/
        cp -f /root/.ssh/*rsa* ${HOME_DASH}/ssh_keys/
    fi
else
	if [ -f /root/.ssh/known_hosts ]
	then
		rm /root/.ssh/known_hosts
	fi
fi
