#!/bin/bash

# while-menu-dialog: a menu driven system information program

DIALOG_CANCEL=1
DIALOG_ESC=255
HEIGHT=0
WIDTH=0

display_result() {
  dialog --title "$1" \
    --no-collapse \
    --msgbox "$result" 0 0
}

while true; do
  exec 3>&1
  selection=$(dialog \
    --backtitle "aniran@aniran.info" \
    --title "Dashboard: Ambiente Hotmart" \
    --clear \
    --cancel-label "Sair" \
    --menu "Selecione uma opcao:" $HEIGHT $WIDTH 4 \
    "1" "Configurar credenciais AWS" \
    "2" "Display Disk Space" \
    "3" "Display Home Space Utilization" \
    2>&1 1>&3)
  exit_status=$?
  exec 3>&-
  case $exit_status in
    $DIALOG_CANCEL)
      clear
      echo "Program terminated."
      return 0
      ;;
    $DIALOG_ESC)
      clear
      echo "Program aborted." >&2
      return 1
      ;;
  esac
  case $selection in
    0 )
      clear
      echo "Program terminated."
      ;;
    1 )
      #result=$(echo "Hostname: $HOSTNAME"; uptime)
      #display_result "System Information"
      dialog --title "Credenciais AWS" --ok-label "Gravar" --cancel-label "Cancelar" --form "Favor preencher os dados:" \
        15 50 0 \
        "Access key ID:"        1 1 "$KEY_ID"  1 20 20 0 \
        "Access key Password:"  2 1 "$KEY_PW"  2 20 40 0 
      ;;
    2 )
      result=$(df -h)
      display_result "Disk Space"
      ;;
    3 )
      if [[ $(id -u) -eq 0 ]]; then
        result=$(du -sh /home/* 2> /dev/null)
        display_result "Home Space Utilization (All Users)"
      else
        result=$(du -sh $HOME 2> /dev/null)
        display_result "Home Space Utilization ($USER)"
      fi
      ;;
  esac
done
