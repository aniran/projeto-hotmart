#!/usr/bin/python

import os, stat

ARQ_MAIN='main.tf'
ARQ_SET_TF_VARS='.set_tf_vars.sh'

arq_tf_vars = open(ARQ_SET_TF_VARS,'w')
arq_tf_vars.write("#!/bin/bash\n\n")
arq_main = open(ARQ_MAIN)
var_nome, var_valor = "",""

for linha in arq_main.readlines():
    if linha.startswith('variable'):
        var_nome = linha.split()[1].strip('"')
    if ("default" in linha) and var_nome:
        var_valor = linha.split()[2].strip('"')
        arq_tf_vars.write("export "+ var_nome +"="+ var_valor +"\n")
        var_nome, var_valor = "",""
    

arq_tf_vars.close()
arq_main.close()

st = os.stat(ARQ_SET_TF_VARS)
os.chmod(ARQ_SET_TF_VARS, st.st_mode | stat.S_IEXEC)
