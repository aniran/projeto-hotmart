variable "path_rsa" {
  description = "Chave RSA AWS"
  default = "/root/.ssh/hm_aws.rsa"
}

variable "path_rsa_pub" {
  description = "Chave RSA publica AWS"
  default = "/root/.ssh/hm_aws.rsa.pub"
}

variable "instance_connection_user" {
  description = "Usuario de conexao com as instancias"
  default = "ubuntu"
}

variable "http_port" {
    description = "Porta para o servidor Web"
    default = 8080
}

resource "aws_key_pair" "auth" {
  key_name   = "hm_rsa_key"
  public_key = "${file(var.path_rsa_pub)}"
}

data "aws_ami" "ubuntu" {
    most_recent = true
    filter {
        name = "name"
        #values = ["ami-ubuntu-16.04-1.10.3-00-1526923973"]
        #values = ["bootnode-vault-consul-ubuntu-2018-04-04T22-28-07Z"]
        values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20180814"]
    }
}

resource "aws_instance" "integracao" {
    count = 1
    ami = "${data.aws_ami.ubuntu.id}"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.auth.key_name}"
    tags { 
        type = "cicd"
        env = "desenv"
    }
    connection {
        user = "${var.instance_connection_user}"
        private_key = "${file(var.path_rsa)}"
    }
    provisioner "remote-exec" {
        inline = ["sudo apt-get -y update && sudo apt-get -y install python"]
        connection {
            type        = "ssh"
            user        = "ubuntu"
            private_key = "${file(var.path_rsa)}"
        }
    }
    vpc_security_group_ids = ["${aws_security_group.instance.id}"]
}

resource "aws_instance" "prod_appserver" {
    count = 1
    ami = "${data.aws_ami.ubuntu.id}"
    instance_type = "t2.micro"
    key_name = "${aws_key_pair.auth.key_name}"
    tags { 
        type = "app"
        env = "prod"
    }
    connection {
        user = "${var.instance_connection_user}"
        private_key = "${file(var.path_rsa)}"
    }
    provisioner "remote-exec" {
        inline = ["sudo apt-get -y update && sudo apt-get -y install python"]
        connection {
            type        = "ssh"
            user        = "ubuntu"
            private_key = "${file(var.path_rsa)}"
        }
    }
    vpc_security_group_ids = ["${aws_security_group.instance.id}"]
}

resource "aws_security_group" "instance" {
    name = "web_server"
}

resource "aws_security_group_rule" "http" {
  type            = "ingress"
  from_port       = "${var.http_port}"
  to_port         = "${var.http_port}"
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.instance.id}"
}

resource "aws_security_group_rule" "ssh" {
  type            = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.instance.id}"
}

resource "aws_security_group_rule" "all_egress" {
  type            = "egress"
  from_port       = 0
  to_port         = 65535
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.instance.id}"
}

resource "aws_vpc" "vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
}
