#!/bin/bash

export HOME_DASH=/var/ambiente

chmod go-rwx /root/.ssh /root/.aws /var/ambiente -R

cp -f ${HOME_DASH}/aws/* /root/.aws && chmod go-rwx /root/.aws -R
cp -f ${HOME_DASH}/ssh_keys/*rsa* /root/.ssh && chmod go-rwx /root/.ssh -R

export CRYPT="-aes-256-cbc"
export AWS_CREDS=/root/.aws/credentials
export AWS_CONFG=/root/.aws/config
export SENHA_PESSOAL=""

for ll in /root/.ssh/*rsa.pub; do
    if [ ! -e "${ll}" ]
    then
        echo "Chaves RSA nao encontradas."
        echo "Gerando novo par de chaves RSA."
        ssh-keygen -t rsa -f /root/.ssh/hm_aws.rsa -q -N ""
    fi
done

echo "";echo "Chaves RSA existentes:"
ls -lah /root/.ssh/*rsa* | grep rsa | awk '{print $9}'

if [ ! -f "${AWS_CREDS}${CRYPT}" ] || [ ! -f "${AWS_CONFG}${CRYPT}" ]
then
    echo "";echo "Nao foram encontrados arquivos de credenciais para o AWS";echo "Executando configuracao do aws cli"
    aws configure
    echo "";echo "Defina uma senha para acesso ao dashboard:"
    read -s SENHA_PESSOAL
    
    echo "";echo "Criptografando credenciais"
    openssl enc -in ${AWS_CREDS} ${CRYPT} -k ${SENHA_PESSOAL} > ${AWS_CREDS}${CRYPT}
    openssl enc -in ${AWS_CONFG} ${CRYPT} -k ${SENHA_PESSOAL} > ${AWS_CONFG}${CRYPT}
    rm -f ${AWS_CREDS} ${AWS_CONFG}
    echo "";echo "Configuracao finalizada"
fi

echo ""; echo "Digite a senha de acesso ao dashboard:"
read -s SENHA_PESSOAL

export AWS_ACCESS_KEY_ID=$(openssl enc -in ${AWS_CREDS}${CRYPT} -d ${CRYPT} -k ${SENHA_PESSOAL} | grep aws_access_key_id | awk '{print $3}')
export AWS_DEFAULT_REGION=$(openssl enc -in ${AWS_CONFG}${CRYPT} -d ${CRYPT} -k ${SENHA_PESSOAL} | grep region | awk '{print $3}')
export AWS_SECRET_ACCESS_KEY=$(openssl enc -in ${AWS_CREDS}${CRYPT} -d ${CRYPT} -k ${SENHA_PESSOAL} | grep aws_secret_access_key | awk '{print $3}')

if [ -z "${AWS_ACCESS_KEY_ID}" ] || [ -z "${AWS_DEFAULT_REGION}" ] || [ -z "${AWS_SECRET_ACCESS_KEY}" ] 
then
    echo "";echo "Ocorreu um erro ao verificar as credenciais"
    exit 1
fi

python build_set_tf_vars.py

source .set_tf_vars.sh

#export TF_LOG=DEBUG
