#!/bin/bash

source /var/ambiente/set_envs.sh

PLAY_BOOK=$1

if [ -z "${PLAY_BOOK}" ]
then
        PLAY_BOOK=site.yml
fi

cp /var/ambiente/ansible /root -R
chmod go-rwx /root/ansible -R
cd /root/ansible
ansible-playbook --private-key ${path_rsa} -i hosts ${PLAY_BOOK}
