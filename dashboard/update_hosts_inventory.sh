#!/bin/sh

HOME_DASH=/var/ambiente

source set_envs.sh

terraform init
terraform apply 

python build_tf_inventory.py

for ll in $(grep 'INICIO_HOSTS_ANSIBLE' /etc/hosts -A999 | grep -v 'INICIO_HOSTS_ANSIBLE' | awk '{print $2}')
do
    ssh-keygen -q -F $ll 1>/dev/null
    if [ "$?" -ne 0 ]
    then 
        ssh-keyscan -H $ll >> /root/.ssh/known_hosts
    fi 
done

./provisioner.sh

cp -f /root/.aws/* ${HOME_DASH}/aws/
cp -f /root/.ssh/*rsa* ${HOME_DASH}/ssh_keys/
