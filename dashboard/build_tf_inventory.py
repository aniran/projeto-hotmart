#!/usr/bin/python

import os,json

PATT_INICIO='#INICIO_HOSTS_ANSIBLE'
ARQ_TFSTATE='terraform.tfstate'
ARQ_HOSTS='/etc/hosts'
ARQ_INVENTORY='ansible/hosts/inventory'

arq_hosts = open(ARQ_HOSTS)
arq_new_hosts = open(ARQ_HOSTS+".new",'w')

for linha in arq_hosts.readlines():
    if PATT_INICIO in linha:
        break
    else:
        arq_new_hosts.write(linha)

arq_new_hosts.close()
arq_hosts.close()

os.system("cat "+ ARQ_HOSTS +".new > "+ ARQ_HOSTS)
os.remove(ARQ_HOSTS+".new")

arq_tfstate = open(ARQ_TFSTATE)
arq_hosts = open(ARQ_HOSTS,'a')
arq_inv = open(ARQ_INVENTORY,'w')

arq_hosts.write(PATT_INICIO +"\n")

data = json.load(arq_tfstate)
grupos = {}

for instancia in data['modules'][0]['resources']:
    if 'aws_instance' in instancia:
        host = '_'.join(instancia.split('.')[1:])
        atributos = data['modules'][0]['resources'][instancia]['primary']['attributes']
        public_ip = atributos['public_ip']
        linha_hosts = public_ip + " " + host
        arq_hosts.write(linha_hosts+"\n")

        for att in atributos:
            if att.startswith('tags.') and ('%' not in att):
                att_nome = att.split('.')[1]
                att_valor = atributos[att]
                grupos.setdefault(att_valor,[]).append(host)

icu = os.environ['instance_connection_user']

for grupo in grupos:
    arq_inv.write("["+grupo+"]\n")

    for h in grupos[grupo]:
        arq_inv.write(h + " ansible_user=" + icu +"\n")
    arq_inv.write("\n")
            
arq_tfstate.close()
arq_hosts.close()
arq_inv.close()
