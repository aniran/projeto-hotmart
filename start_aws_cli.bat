@echo off

git --version | findstr /R /C:"git version" 
if %errorlevel% neq 0 (
    echo Git nao encontrado.
    pause
    goto FIM
)

docker --version | findstr /R /C:"Docker version"  
if %errorlevel% neq 0 (
	echo Docker engine nao encontrado.
    pause
    goto FIM
)

git pull
docker build --rm -t aws-cli -f Dockerfile .
docker run -ti --rm --name aws-cli -v %cd%\dashboard:/var/ambiente aws-cli %1

:FIM
